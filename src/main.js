/*


*/


// OWNER OF THE BOT
// Change here by right clicking on your name and pressing "Copy id"
// You need Developer Mode enabled for this action. Google it.
const owner_id="154778665970827265";

const credentials=require('discord-credentials.js');
const Discord = require('discord.js');
const client = new Discord.Client();

let channel_configs = {}

// Bot is logged in and almost ready
client.on('ready', () => {
	local_user_id=client.user.id;

	var a=Array.from(client.guilds.values());

	console.log(`Logged in as ${client.user.tag}!`);
	console.log(`This bot is in ${a.length} channel${a.length!==1?'s':''}.`);

	for (var i = a.length - 1; i >= 0; --i){
		if((typeof channel_configs[a[i].id]) === 'undefined') {
			channel_configs[a[i].id] = {
				"listen-and-assign":null
			}

			console.log("Added new channel to config");
		}
	}

	/*var new_discord_channels=[];

	for (var i = a.length - 1; i >= 0; i--) {

		// Check for new channels. Add them up.
		if((typeof twitch_channel_listener.servers[a[i].id])==='undefined'||twitch_channel_listener.servers[a[i].id].isEmpty()) {

			twitch_channel_listener.servers[a[i].id]={
				interact_mode:false,
				tell_everyone:true,
				warn_owner:false,
				streamer_announcements:{},
				admin_roles:{},
				announce_channel:"",
				bot_roles:[]
			};

			console.log(client.guilds.get(a[i].id).ownerID);

			if(new_discord_channels.indexOf(client.guilds.get(a[i].id).ownerID)===-1){
				new_discord_channels.push(client.guilds.get(a[i].id).ownerID);
			}
		}


	}
*/
	// Send a welcome message to new guilds that we're in.
/*	new_discord_channels.forEach((_owners)=>{
		client.users.get(_owners).send(welcome_message.replace("{local_user_id}",local_user_id));
	});*/

	// check twitch every 10 minutes
	// client_interval=client.setTimeout(check_twitch_channels,5000);
});

// Handle messages when they are detected
client.on('message',(_msg)=>{

	// IGNORE THESE MESSAGES
	if(_msg.author.id===local_user_id||_msg.author.bot||(_msg.guild===null&&_msg.author.id!=="154778665970827265")){return;}

	var end_message=null;
	var dm=false;

	// Is user an admin, and is the bot being @'d '
	if(_msg.member.hasPermission("ADMINISTRATOR")&&_msg.content.toLowerCase().startsWith("<@"+local_user_id+"> ")) {
		// Clear the reference to the bot and continue parsing message
		var txt=_msg.content.toLowerCase().replace(/^\<@\d+\> /,'');

		// Credit: https://stackoverflow.com/questions/24069344/split-spaces-avoiding-double-quoted-js-strings-from-a-b-c-d-to-a
		let msg_arguments=txt.match(/"(?:\\"|\\\\|[^"])*"|\S+/g);

		console.log(msg_arguments);
		if(msg_arguments[0] === "listen-and-assign"){
			/*
			 * Arguments:
			 * 0 - id of channel to listen on (if empty string, use location of message)
			 * 1 - message to listen for (If multiple words, use a double quote to treat as one argument)
			 * 2 - Role to give to user when they satisfy the message
			 * 3 - (optional) use strict behavior (delete messages if they don't align to message to listen for)
			 *
			 * NOTE: Bot deletes messages that it listens for when it is satisfied.
			 */

			 // Validate that the role exists!
			 let guildRole = _msg.guild.roles.get(msg_arguments[3]);

			 if(!guildRole){
			 	guildRole = _msg.guild.roles.find( (_roles) => _roles.name.toLowerCase() === msg_arguments[3] );

			 	// console.log(_msg.guild.roles);
			 	console.log(guildRole);
			 }

			 if(!guildRole){
			 	end_message="The role you provided does not exist!";
			 }else{
				 channel_configs[_msg.guild.id]["listen-and-assign"] = {
				 	channel:msg_arguments[1],
				 	message:msg_arguments[2].toLowerCase(),
				 	role: guildRole
				 }

				 end_message="Listening for messages.";
			 }



		}


		if(!end_message){
			return;
		}

		if(dm){
			setTimeout(function(){
				_msg.author.send(end_message);
			},1000);
		}else{
			_msg.channel.startTyping();
			setTimeout(function(){
				// var t=msg.guild.emojis.map(e=>e.toString()).join(" ");
				_msg.channel.stopTyping();
				_msg.channel.send(end_message).catch((_error)=>{
					_msg.author.send(end_message+"\n\nThis message was personally sent because I can't post in the channel you posted on.");
				});
			},2500);
		}
	}

});

client.on('error', console.error);


client.login(credentials.dabbing_bot.discord_key);