/*
 *	Discord Credential File
 *
 * In order to make the bot work, you will need to replace these fields with your credentials.
 *
 * owner_id - This is the unique identifier on discord to identify you as the owner of this bot.
 *		To acquire this id, you will need to enable developer mode on discord, and then right
 *		click on your name and select "copy id", and paste it here. 
 *
 * discord_key - this key is provided when you register a bot account on the discord
 *		developer site.
 *
 * twitch_key - this key is provided when you register on the twitch developer site.
 */

module.exports={
	owner_id:'REPLACE-ME',
	discord_key:'REPLACE-ME',
	twitch_key:'REPLACE-ME'
};
